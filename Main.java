import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    static final int LIMIT = 5;
    static String mask  = "*";
    public static void main(String[] args) throws IOException {

        String name = "";
        String userName = "";
        String email = "";

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("이름을 입력해 주세요: ");
        name = br.readLine();
        System.out.println("아이디를 입력해 주세요: ");
        userName = br.readLine();
        System.out.println("이메일을 입력해 주세요: ");
        email = br.readLine();

        name = masking(name.trim());
        userName = masking(userName.trim());

        String domain = getDomain(email.trim());
        email = masking(getEmailWithoutDomain(email.trim()));
        email += domain;

        System.out.println("name: "+name+", userName: "+userName+", email: "+email);
    }
    static String masking(String str) {
        String[] arr = str.split("");
        StringBuilder ret = new StringBuilder();
        if (str.length() >= LIMIT) {
            for (int i = 0; i < arr.length; i++) {
                if (i == 0 || i == 1 || i == arr.length - 1) {
                    ret.append(arr[i]);
                    continue;
                }
                arr[i] = mask;
                ret.append(arr[i]);
            }
        } else {
            for (int i = 0; i < arr.length; i++) {
                if (i == 0 || i == arr.length - 1) {
                    ret.append(mask);
                    continue;
                }
                ret.append(arr[i]);
            }
        }
        return ret.toString();
    }
    static String getEmailWithoutDomain(String str) {
        String[] arr = str.split("");
        StringBuilder email = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals("@")) break;
            email.append(arr[i]);
        }
        return email.toString();
    }
    static String getDomain(String str) {
        String[] arr = str.split("");
        StringBuilder domain = new StringBuilder();
        int domainStart = str.indexOf("@");
        for (int i = domainStart; i < arr.length; i++) {
            domain.append(arr[i]);
        }
        return domain.toString();
    }
}
